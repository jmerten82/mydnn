readme.txt
Author: Julian Merten (INAF-OAS Bologna, julian.merten@inaf.it, http://www.julianmerten.net)
Location: mydnn/reproducible_science/merten18/

This readme file gives you a quick outline on how to reproduce, step-by-step the results of:

"On the dissection of degenerate cosmologies with machine learning" by
Julian Merten, Carlo Giocoli, Marco Baldi, Massimo Meneghetti, Austin Peel, Florian Lalande, Jean-Luc Starck and Valeria Pettorino.

**Step 1: Download the Dustgrain convergence maps as FITS files
For this step you will need
    -- Location: https://apps.difa.unibo.it/files/people/Str957-cluster/WeakLensing/C2PAP/downscaled/
    -- Download the following tarballs:
        - LCDM_PowerBornApp.tar.gz
        - fR4_0.3eV_PowerBornApp.tar.gz
        - fR4_PowerBornApp.tar.gz
        - fR5_0.1eV_PowerBornApp.tar.gz
        - fR5_0.15eV_PowerBornApp.tar.gz
        - fR5_PowerBornApp.tar.gz
        - fR6_0.06eV_PowerBornApp.tar.gz
        - fR6_0.1eV_PowerBornApp.tar.gz
        - fR6_PowerBornApp.tar.gz
    -- Extract into the same directory, to which we will refer to as 'MAPDIR'

From now on we assume that you have a working Python 2.7(3.X) environment with numpy and Jupyter frontend.
All of the below can certainly be done with stand-alone python scripts, e.g. run in a screen environment. In fact, most of our production runs are. The notebooks are meant to give you an interactive overview of the different steps. Feel free to copy and paste from them and produce your own analysis chain. 


**Step 2: Create a master HDF5 file for the following analyses. This is a bit of an overhead since the data is already on disk as FITS, but esp. for the on-the-fly data generation for the training of large networks, the HDF5 solution is more efficient. Also, this avoid a lot of headaches later when keeping track of the different maps, models and redshifts.
For the following you need: h5py, pyfits
    -- Open the 'prepare_raw_data.ipynb' notebook.
    -- In input line 3 of the notebook set 'input_dir' to MAPDIR created in step 1.
    -- Set 'output_h5' to a location and filename of your choice. Keep in mind that the final hdf5 file will take almost 200Gb of disk space. We will refer to this file as H5MASTER from now on. 
    -- <SHIFT>-<ENTER> through the notebook and feel free to modify and extend. 

**Step 3: Extract classical features from the mass maps. The output of this will be a wnd-charm .fit file (nothing to do with FITS, it's in fact ASCII), which we still use for continuity reasons. This file stores all features in a specifically formatted ASCII file. 
For the following you need: lenstools, astropy, h5py, scipy and keras (here actually just for its fancy progress bar)
    -- Open the 'extract_classical_features.ipynb' notebook.
    -- Set 'master_file' in input line 4 to H5MASTER created in step 2.
    -- Make your choice for set (train,valdiation,test) and redshift in line 4.
    -- Finally pick the output filename, also in line 4.
    
**Step 4: Extract wnd-charm features from the maps. This step is a little more complicated since it uses the C++ version of wnd-charm. 
For the following you need: h5py, pyfits, wnd-charm (modified with our own)
    -- Open the 'prepare_data_for_wndcharm.ipynb' notebook.
    -- Set 'master_file' to H5MASTER and 'out_dir' to an existing directory where you want to place the images to be analysed by wnd-charm. We will refer to this directory as WNDIMGDIR 
    -- As before, you need to make a choice for the classes, the set and the redshift you want to analyse. 
    -- Compile wnd-charm with the modified cmatrix.cpp file that is provided in the same folder as this readme.txt file.
    -- Point wnd-charm to outdir and run 'wndchrm train -ml WNDIMGDIR ./my_result.fit '

**Step 5: Analyse Fisher weights. Given a valid feature .fit file, you can analyse the Fisher weights for the sample. 
For the following you need: mydnn (this repository)
    -- Open 'extract_fisher_weights.ipynb' 
    -- Set the correct path for your mydnn installation in the import fields. 
    -- Set the correct paths to your fit files. 

**Step 6: Distance-based classification. Use either a WNN or WND distance to classify a train/test .fit combination. 
For the following you need: mydnn (this repository)
    -- Open 'classify_distance.ipynb' 
    -- Point the routine to a train and test .fit file you have e.g. created in step 3 or 4.
    
**Step 7: Training a neural network based on fixed features. 
For the following you need: mydnn, keras
    -- Open 'train_fixed_feature_nn_classifier.ipynp'. This notebook shows how to train a neural network on both classical and wnd-charm features. This can be altered to classify any of you train/test set combiations. 
    -- Point to the relevant train and test feature files in input field 2. 
    
**Step 8: Train a CNN. Here you have a couple of options in case you a) Train a network on a CPU or single GPU, b) Train a network on multiple GPUs or c) you train a network on a large data set which does not fit into the main memory of your machine. 
For the following you need: mydnn, keras

**Step 9: Visualise the results. This notebook shows you how to analyse and visualise the results for every kind of classification method. Work through the notebook and load the relevant models and data generators.
For the following you need: mydnn, keras

**Step 10: Visualise filter responses in the network. 
For the following you need: keras
    -- Refer to "https://github.com/keras-team/keras/blob/master/examples/conv_filter_visualization.py" for an example gradient ascend example. 
    -- We also attach our rough routine which is based on the above with "./scripts/extract_filters.py"

    
    