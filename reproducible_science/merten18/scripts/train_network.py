import sys
sys.path.append('/home/jmerten/codes/mydnn/mydnn/cnn/')
sys.path.append('/home/jmerten/codes/mydnn/mydnn/utils/')
import numpy as np
import tensorflow as tf
from models import *
from mycallbacks import MultiGPUCheckpointCallback
from datasets import load_dataset as ld
from keras.callbacks import CSVLogger
from keras.utils import multi_gpu_model
from keras.preprocessing.image import ImageDataGenerator

#train_classes4 = np.array(['/z10/f5/train/','/z10/f5_015ev/train/','/z10/f5_01ev/train/','/z10/lcdm/train/'])
#test_classes4 = np.array(['/z10/f5/test/','/z10/f5_015ev/test/','/z10/f5_01ev/test/','/z10/lcdm/test/'])
train_classes_z1 = np.array(['/z10/f4/train/','/z10/f4_03ev/train/','/z10/f5/train/','/z10/f5_015ev/train/','/z10/f5_01ev/train/','/z10/f6/train/','/z10/f6_006ev/train/','/z10/f6_01ev/train/','/z10/lcdm/train/'])
test_classes_z1 = np.array(['/z10/f4/test/','/z10/f4_03ev/test/','/z10/f5/test/','/z10/f5_015ev/test/','/z10/f5_01ev/test/','/z10/f6/test/','/z10/f6_006ev/test/','/z10/f6_01ev/test/','/z10/lcdm/test/'])
train_classes_z2 = np.array(['/z20/f4/train/','/z20/f4_03ev/train/','/z20/f5/train/','/z20/f5_015ev/train/','/z20/f5_01ev/train/','/z20/f6/train/','/z20/f6_006ev/train/','/z20/f6_01ev/train/','/z20/lcdm/train/'])
test_classes_z2 = np.array(['/z20/f4/test/','/z20/f4_03ev/test/','/z20/f5/test/','/z20/f5_015ev/test/','/z20/f5_01ev/test/','/z20/f6/test/','/z20/f6_006ev/test/','/z20/f6_01ev/test/','/z20/lcdm/test/'])
train_classes_z05 = np.array(['/z05/f4/train/','/z05/f4_03ev/train/','/z05/f5/train/','/z05/f5_015ev/train/','/z05/f5_01ev/train/','/z05/f6/train/','/z05/f6_006ev/train/','/z05/f6_01ev/train/','/z05/lcdm/train/'])
test_classes_z05 = np.array(['/z05/f4/test/','/z05/f4_03ev/test/','/z05/f5/test/','/z05/f5_015ev/test/','/z05/f5_01ev/test/','/z05/f6/test/','/z05/f6_006ev/test/','/z05/f6_01ev/test/','/z05/lcdm/test/'])
train_classes_z15 = np.array(['/z15/f4/train/','/z15/f4_03ev/train/','/z15/f5/train/','/z15/f5_015ev/train/','/z15/f5_01ev/train/','/z15/f6/train/','/z15/f6_006ev/train/','/z15/f6_01ev/train/','/z15/lcdm/train/'])
test_classes_z15 = np.array(['/z15/f4/test/','/z15/f4_03ev/test/','/z15/f5/test/','/z15/f5_015ev/test/','/z15/f5_01ev/test/','/z15/f6/test/','/z15/f6_006ev/test/','/z15/f6_01ev/test/','/z15/lcdm/test/'])

x_train, y_train = ld('/dati1/jmerten//dustgrain_pathfinder/paper_data/dustgrain_train_test_classify_split_256_slices.h5',classes=train_classes_z1,scale=False)
x_test, y_test = ld('/dati1/jmerten//dustgrain_pathfinder/paper_data/dustgrain_train_test_classify_split_256_slices.h5',classes=test_classes_z1,scale=False)

datagen = ImageDataGenerator()
#x_train /= 1.e13
#x_test /= 1.e13

try:
    tf.Session()
except Exception:
    pass

my_model = DIBARE(classes=9,bn_momentum=.0,FC1=0,FC2=0,feature_dropout=.33,num_layersA=1,num_layersB=1,num_layersC=1,leak=.03)
my_gpu_model = multi_gpu_model(my_model,2)
my_gpu_model.compile(metrics=['accuracy'],optimizer='adam',loss=['categorical_crossentropy'])
model_save = MultiGPUCheckpointCallback(filepath='/dati1/jmerten/dustgrain_pathfinder/cnn/classification/DIBARE_1_1_1_p33.h5',base_model=my_model,monitor='val_loss',save_best_only=True,mode='min',verbose=20)
model_log = CSVLogger('/dati1/jmerten/dustgrain_pathfinder/cnn/classification/DIBARE_1_1_1_p33.log',append=True)
my_gpu_model.fit_generator(datagen.flow(x_train,y_train,batch_size=64),validation_data=datagen.flow(x_test,y_test,batch_size=128),steps_per_epoch=len(x_train)/128,validation_steps=len(x_test)/128,epochs=50,callbacks=[model_log,model_save])


#x_train, y_train = ld('/dati1/jmerten//dustgrain_pathfinder/dustgrain_train_test_classify_split_256_slices.h5',classes=train_classes_z15,scale=False)
#x_test, y_test = ld('/dati1/jmerten//dustgrain_pathfinder/dustgrain_train_test_classify_split_256_slices.h5',classes=test_classes_z15,scale=False)

#my_model = InceptionV4(classes=9,bn_momentum=.0,FC1=0,FC2=0,feature_dropout=.33,num_layersA=1,num_layersB=1,num_layersC=1,leak=.03)
#my_gpu_model = multi_gpu_model(my_model,2)
#my_gpu_model.compile(metrics=['accuracy'],optimizer='adam',loss=['categorical_crossentropy'])
#model_save = MultiGPUCheckpointCallback(filepath='/dati1/jmerten/dustgrain_pathfinder/cnn/classification/I4_z15_1_1_1_p33.h5',base_model=my_model,monitor='val_loss',save_best_only=True,mode='min',verbose=20)
#model_log = CSVLogger('/dati1/jmerten/dustgrain_pathfinder/cnn/classification/I4_z15_1_1_1_p33.log',append=True)
#my_gpu_model.fit_generator(datagen.flow(x_train,y_train,batch_size=64),validation_data=datagen.flow(x_test,y_test,batch_size=128),steps_per_epoch=len(x_train)/128,validation_steps=len(x_test)/128,epochs=50,callbacks=[model_log,model_save])
