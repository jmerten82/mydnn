import sys
sys.path.append('/home/jmerten/codes/mydnn/mydnn/cnn/')
sys.path.append('/home/jmerten/codes/mydnn/mydnn/utils/')
import numpy as np
import tensorflow as tf
from models import *
from datasets import *
from keras.callbacks import CSVLogger
from keras.utils import multi_gpu_model
from keras.preprocessing.image import ImageDataGenerator
import os
os.environ["CUDA_VISIBLE_DEVICES"]="0"


try:
    tf.Session()
except Exception:
    pass

my_data = Dustgrain_H5_datagenerator('/dati1/jmerten/dustgrain_pathfinder/dustgrain_train_test_classify_split_256_slices_4z_channels.h5',sample='train',channels=np.array((0,1,2,3)),classes=np.array(('f5','f5_015ev','f5_01ev','lcdm')),batch_size=64)

my_data2 = Dustgrain_H5_datagenerator('/dati1/jmerten/dustgrain_pathfinder/dustgrain_train_test_classify_split_256_slices_4z_channels.h5',sample='test',channels=np.array((0,1,2,3)),classes=np.array(('f5','f5_015ev','f5_01ev','lcdm')),batch_size=64)


my_model = InceptionV4(input_shape=(256,256,4),bn_momentum=.0,classes=4,feature_dropout=.33,num_layersA=1,num_layersB=1,num_layersC=1,leak=.03)
my_model.compile(metrics=['accuracy'],optimizer='adam',loss=['categorical_crossentropy'])
log_callback = keras.callbacks.CSVLogger('/dati1/jmerten/dustgrain_pathfinder/paper_data/final_models/I4_3_3_3_p33_4_classes_4channel.txt')
model_save_callback = keras.callbacks.ModelCheckpoint(filepath='/dati1/jmerten/dustgrain_pathfinder/paper_data/final_models/I4_3_3_3_p33_4_classes_4channel.h5',verbose=1,save_best_only=True,mode='min')
my_model.fit_generator(my_data,validation_data=my_data2,epochs=20,callbacks=[log_callback,model_save_callback])
