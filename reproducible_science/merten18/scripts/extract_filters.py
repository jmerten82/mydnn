import numpy as np
import time
from keras import backend as K
import keras
K.set_learning_phase(1)
from scipy.misc import imsave
import os
import matplotlib.pyplot as plt
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"] = ""


model_path = '/dati1/jmerten/dustgrain_pathfinder/cnn/classification/I4_1_1_1_p33.h5'

def deprocess_image(x):
        # normalize tensor: center on 0., ensure std is 0.1
        x -= x.mean()
        x /= (x.std() + K.epsilon())
        x *= 0.1

        # clip to [0, 1]
        x += 0.5
        x = np.clip(x, 0, 1)
        return x

def normalize(x):
        # utility function to normalize a tensor by its L2 norm
        return x / (K.sqrt(K.mean(K.square(x))) + K.epsilon())

my_model = keras.models.load_model(model_path)
layer_dict = dict([(layer.name, layer) for layer in my_model.layers[1:]])

layer_dict = dict([(layer.name, layer) for layer in my_model.layers[1:]])
img_width=256
img_height = 256
for index in range(42,43):
        layer_name = "conv2d_" +str(index)
        input_img = my_model.input
        layer_output = layer_dict[layer_name].output
        file_base = "/dati1/jmerten/dustgrain_pathfinder/paper_data/fig/filters/" + layer_name + "_"
        print layer_name

        kept_filters = []
        for filter_index in range(0,int(layer_output.shape[3])):
                # we only scan through the first 200 filters,
                # but there are actually 512 of them
                print('Processing filter %d' % filter_index)
                start_time = time.time()
                
                # we build a loss function that maximizes the activation
                # of the nth filter of the layer considered
                if K.image_data_format() == 'channels_first':
                        loss = K.mean(layer_output[:, filter_index, :, :])
                else:
                        loss = K.mean(layer_output[:, :, :, filter_index])
                
                # we compute the gradient of the input picture wrt this loss
                grads = K.gradients(loss, input_img)[0]
                        
                # normalization trick: we normalize the gradient
                grads = normalize(grads)
                        
                # this function returns the loss and grads given the input picture
                iterate = K.function([input_img], [loss, grads])
                        
                # step size for gradient ascent
                step = 1.
                        
                # we start from a gray image with some random noise
                if K.image_data_format() == 'channels_first':
                        input_img_data = np.random.random((1, 3, img_width, img_height))
                else:
                        input_img_data = np.random.random((1, img_width, img_height, 1))
                input_img_data = (input_img_data - 0.5) * 20 + 128
                        
                # we run gradient ascent for 20 steps
                for i in range(20):
                        loss_value, grads_value = iterate([input_img_data])
                        input_img_data += grads_value * step
                                
                        print('Current loss value:', loss_value)
                        if loss_value <= 0.:
                                # some filters get stuck to 0, we can skip them
                                break
                                        
                # decode the resulting input image
                if loss_value > 0:
                        img = deprocess_image(input_img_data[0])
                        kept_filters.append((img, loss_value))
                        end_time = time.time()
                        print('Filter %d processed in %ds' % (filter_index, end_time - start_time))
                                        
        for x in range(0,np.shape(kept_filters)[0]):
                file = file_base + str(x+1) +".png"
                plt.imsave(file,kept_filters[x][0].T[0],cmap='binary_r')
