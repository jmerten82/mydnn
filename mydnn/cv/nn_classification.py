import keras
from keras.models import Sequential, Model
from keras.layers import Input, Dense, Activation, Dropout, BatchNormalization
from keras.layers.advanced_activations import LeakyReLU


def dense_classifier_for_cv_weights(num_weights=2919,classes=10,FC1=1024,FC2=128,FC3=0,feature_dropout= 0.,FC1_dropout=.5,FC2_dropout=.25, FC3_dropout = .1, bn=False,leak=0.,bn_momentum=.99):


    #Input layer
    cv_weights = Input(shape=(num_weights,))

    #Initial feature dropout
    x = Dropout(FC1_dropout)(cv_weights)

    #First FC_layer, this one might be big
    x = Dense(FC1)(cv_weights)
    if(bn):
        BatchNormalization(momentum=bn_momentum,scale=False)(x)
    if(leak > 0.):
        x = LeakyReLU(leak)(x)
    elif(leak == 0.):
        x = Activation('relu')(x) 
    x = Dropout(FC1_dropout)(x)

    #If wanted, second FC layer
    if(FC2 > 0):
        x = Dense(FC2)(x)
        if(bn):
            BatchNormalization(momentum=bn_momentum,scale=False)(x)
        if(leak > 0.):
            x = LeakyReLU(leak)(x)
        elif(leak == 0.):
            x = Activation('relu')(x) 
    x = Dropout(FC2_dropout)(x)

    #If wanted, third FC layer
    if(FC3 > 0):
        x = Dense(FC3)(x)
        if(bn):
            BatchNormalization(momentum=bn_momentum,scale=False)(x)
        if(leak > 0.):
            x = LeakyReLU(leak)(x)
        elif(leak == 0.):
            x = Activation('relu')(x) 
    x = Dropout(FC3_dropout)(x)

    #Classification via softmax
    x = Dense(classes, activation='softmax')(x)

    model = Model(cv_weights,x,name='CV-Weight-Classification')
    return model
    
