import re
import numpy as np
import numpy.ma as ma
from keras.utils import to_categorical
import h5py as h5

def save_classifcation_to_h5(filename,classification,class_labels,feature_labels):
    h5file = h5.File(filename,mode='w')

    for index in range(0,np.shape(classification)[0]):                
        current_grp = h5file.create_group(class_labels[index])
        for sample_index in range(0,np.shape(classification)[1]):
            name = feature_labels[index,sample_index]
            if(name.rfind('/')):
                name = name[name.rfind('/')+1:]
            current_set = current_grp.create_dataset(name,data=classification[index,sample_index])

    h5file.close()
    


class wndcharm_features (object):
    
    #This class reads from a wndcharm fit file and saves all information about classes, feature values, feature names and allows for subsequent calculations
    
    def __init__(self, filename):
        #Constructor, needs filename of fit file
        
        feature_file = open(filename)
        
        #Reading fitfile preamble
        line = feature_file.next()
        self.num_classes, self.feature_version = re.match('^(\S+)\s*(\S+)?$', line.strip()).group(1, 2)
        self.num_classes = int(self.num_classes)
        self.feature_version = float(self.feature_version)
        self.num_features = int(feature_file.next())
        self.num_samples = int(feature_file.next()) / self.num_classes                
        
        #Reading feature names from fitfile
        self.feature_names = np.empty([0],dtype='string')
        for index in range(0,self.num_features):
            self.feature_names =  np.append(self.feature_names,feature_file.next().strip())
            
        #Hopping over one new line
        line = feature_file.next()
        
        #Reading class names
        self.class_names = np.empty([0],dtype='string')
        for index in range(0,self.num_classes):
            self.class_names =  np.append(self.class_names,feature_file.next().strip())
            
        #Finally, reading feature values and sample names
        self.feature_weights = np.zeros([self.num_classes,self.num_samples,self.num_features],dtype='float')
        self.sample_names = np.zeros([self.num_classes,self.num_samples],dtype='U200')
        
        for current_class in range(0,self.num_classes):
            for sample in range(0,self.num_samples):
                line = feature_file.next()
                features_string, class_index_string  = line.strip().rsplit( " ", 1 )
                class_index = int(class_index_string) -1
                features = np.array([float(val) for val in features_string.split()])
                line = feature_file.next()
                self.sample_names[class_index][sample] = str(line.strip())
                self.feature_weights[class_index][sample] = features
                
        self.fisher_weights = np.zeros([self.num_features],dtype='float')+1.
        self.feature_shifts = np.zeros([self.num_features,2],dtype='float')
        self.feature_shifts[:,1] = 1.
        self.shift_factor = 1.

        self.validation_samples = 0
        self.validation_classes = 0
        
    def normalise(self,factor=1.):
        for index in range(0,self.num_features):
            min = np.min(self.feature_weights[:,:,index])
            self.feature_shifts[index,0] = min
            self.feature_weights[:,:,index] -= min
            max = np.max(self.feature_weights[:,:,index])
            self.feature_shifts[index,1] = max
            if(max != 0.):
                self.feature_weights[:,:,index] /= max
            else:
                self.feature_weights[:,:,index] = 0.
        self.feature_weights *= factor
        self.shift_factor = factor
                    
    def calculate_fisher(self):
        for index in range(0,self.num_features):
            feature_average = np.mean(self.feature_weights[:,:,index])
            feature_class_average = np.zeros([self.num_classes],dtype='float')
            feature_class_variance = np.zeros([self.num_classes],dtype='float')
            for current_class in range(0,self.num_classes):
                feature_class_average[current_class] = np.mean(self.feature_weights[current_class,:,index])
                feature_class_variance[current_class] = np.var(self.feature_weights[current_class,:,index])
                divide = np.mean(feature_class_variance)
            if(divide == 0.):
                current_result = 0.
            else:
                current_result = np.sum((feature_average-feature_class_average)**2.)/np.mean(feature_class_variance)
            self.fisher_weights[index] = current_result / (self.num_classes -1)


    def load_validation_set_fromfit(self, filename):
        feature_file = open(filename)
        
        #Reading fitfile preamble
        line = feature_file.next()
        self.validation_classes, self.val_feature_version = re.match('^(\S+)\s*(\S+)?$', line.strip()).group(1, 2)
        self.validation_classes = int(self.validation_classes)
        self.val_feature_version = float(self.val_feature_version)
        self.val_feature_num = int(feature_file.next())
        if(self.val_feature_version != self.feature_version):
            raise ValueError('Feature version not compatible.')
        if(self.val_feature_num != self.num_features):
            raise ValueError('Unequal number of features.')

        self.validation_samples = int(feature_file.next()) / self.validation_classes                
        
        #Skipping over feature names
        for index in range(0,self.num_features):
            feature_file.next()
            
        #Hopping over one new line
        line = feature_file.next()
        
        #Reading class names
        self.validation_class_names = np.empty([0],dtype='string')
        for index in range(0,self.validation_classes):
            self.validation_class_names =  np.append(self.validation_class_names,feature_file.next().strip())
            
        #Finally, reading feature values and sample names
        self.validation_weights = np.zeros([self.validation_classes,self.validation_samples,self.num_features],dtype='float')
        self.validation_sample_names = np.zeros([self.validation_classes,self.validation_samples],dtype='U200')
        
        for current_class in range(0,self.validation_classes):
            for sample in range(0,self.validation_samples):
                line = feature_file.next()
                features_string, class_index_string  = line.strip().rsplit( " ", 1 )
                class_index = int(class_index_string) -1
                features = np.array([float(val) for val in features_string.split()])
                line = feature_file.next()
                self.validation_sample_names[class_index][sample] = str(line.strip())
                self.validation_weights[class_index][sample] = features

        #Renormalising validation set according to training set
        self.validation_weights -= self.feature_shifts[:,0]
        self.validation_weights /= self.feature_shifts[:,1]

        self.validation_weights[np.isnan(self.validation_weights)] = 0.
        self.validation_weights[np.isinf(self.validation_weights)] = 0.
        self.validation_weights *= self.shift_factor


    def classify_via_wnd(self, p = -5.,feature_fraction=.3,external_data=None,save_to=None,prob='sim'):

        if(external_data != None):
            if(np.shape(external_data)[-1] != self.num_features):
                raise RuntimeError('Not going to work with these features.')

            #Bringing input into right shape
            classify = np.reshape(external_data,(1,-1,self.num_features))
            ground_truth = np.array(['unknown'])

            #Renormalising according to test set
            classify -= self.feature_shifts[:,0]
            classify /= self.feature_shifts[:,1]
            
        else:
            classify = self.validation_weights
            ground_truth = self.validation_class_names
        classification = np.zeros([np.shape(classify)[0],np.shape(classify)[1],self.num_classes],dtype='float')
        truth = []

        mask = self.fisher_weights > np.percentile(self.fisher_weights,(1.-feature_fraction)*100)
        working_weights = self.fisher_weights[mask]
        working_features = (self.feature_weights[:,:,mask])*working_weights 
            
        collisions = np.zeros(len(ground_truth),dtype='int')
        np.seterr(all='raise')
        for current_class in range(0,len(ground_truth)):
            correct = 0
            print 'Currently validating: ', ground_truth[current_class], '\t ', np.shape(classify)[1], ' samples.'

            for current_sample in range(0,np.shape(classify)[1]):
                print ground_truth[current_class], '\t', current_class+1, ' / ', len(ground_truth), '\t', current_sample+1, ' / ', np.shape(classify)[1]
                working_classify = (classify[current_class,current_sample,:][mask])*working_weights
                try:
                    distances = np.sum(np.sum((working_classify - working_features)**2,axis=2)**p,axis=1) / self.num_samples
                except FloatingPointError:
                    collisions[current_class] += 1
                    np.seterr(all='ignore')
                    distances = np.sum(np.sum((working_classify - working_features)**2,axis=2)**p,axis=1) / self.num_samples
                    another_mask = np.isinf(distances)
                    distances = np.zeros_like(distances)
                    distances[another_mask] = 1.
                    np.seterr(all='raise')
                    
                if(prob == 'sim'):    
                    classification[current_class,current_sample] = distances/np.sum(distances)
                else:
                    current_max = np.max(distances)
                    current_min = np.min(distances)
                    print distances
                    try:
                        classification[current_class,current_sample] = np.exp(distances-current_max)/np.sum(np.exp(distances-current_max))

                    except FloatingPointError:
                        classification[current_class,current_sample] = np.exp(distances-current_min)/np.sum(np.exp(distances-current_min))
                            
                truth.append(ground_truth[current_class])
                classified_class = self.class_names[np.argmax(classification[current_class,current_sample])]
                if(classified_class == ground_truth[current_class]):
                    correct += 1
                print classification[current_class,current_sample], classified_class
            print 'Class success rate: ', correct, '/', np.shape(classify)[1] 

        for index in range(0,len(ground_truth)):
            print collisions[index], ' collissions in ', ground_truth[index]
        np.seterr(all='warn')

        truth = np.array(truth)

        if(save_to != None):
            if(external_data != None):
                mock_names = []
                for index in range(0,np.shape(classification[1])):
                    mock_names.append('mystery_item'+str(index))
                mock_names = np.array(mock_names)
                    
                save_classifcation_to_h5(save_to,classification,ground_truth,mock_names)
            else:
                save_classifcation_to_h5(save_to,classification,self.validation_class_names,self.validation_sample_names)
                    
        return classification, truth 

    def classify_via_wnn(self,feature_fraction=.3,external_data=None,save_to = None):

        if(external_data != None):
            if(np.shape(external_data)[-1] != self.num_features):
                raise RuntimeError('Not going to work with these features.')

            #Bringing input into right shape
            classify = np.reshape(external_data,(1,-1,self.num_features))
            ground_truth = np.array(['unknown'])

            #Renormalising according to test set
            classify -= self.feature_shifts[:,0]
            classify /= self.feature_shifts[:,1]
            
        else:
            classify = self.validation_weights
            ground_truth = self.validation_class_names

        classification = np.zeros([np.shape(classify)[0],np.shape(classify)[1],self.num_classes],dtype='float')
        truth = []
        mask = self.fisher_weights > np.percentile(self.fisher_weights,(1.-feature_fraction)*100)
        working_weights = self.fisher_weights[mask]
        working_features = (self.feature_weights[:,:,mask])*working_weights 

        collisions = np.zeros(len(ground_truth),dtype='int')
        np.seterr(all='raise')
        for current_class in range(0,len(ground_truth)):
            correct = 0
            print 'Currently validating: ', ground_truth[current_class], '\t ', np.shape(classify)[1], ' samples.'
            
            for current_sample in range(0,np.shape(classify)[1]):
                print ground_truth[current_class], '\t', current_class+1, ' / ', len(ground_truth), '\t', current_sample+1, ' / ', np.shape(classify)[1]
                working_classify = (classify[current_class,current_sample,:][mask])*working_weights
                try:
                    distances = np.min(np.sum((working_classify - working_feature)**2,axis=2),axis=1)
                except FloatingPointError:
                    collisions[current_class] += 1
                    np.seterr(all='ignore')
                    distances = np.min(np.sum((working_classify - working_feature)**2.,axis=2),axis=1)
                    another_mask = np.isinf(distances)
                    distances = np.zeros_like(distances)
                    distances[another_mask] = 1.
                    np.seterr(all='raise')

                if(prob == 'sim'):    
                    classification[current_class,current_sample] = distances/np.sum(distances)
                else:
                    current_max = np.max(distances)
                    current_min = np.min(distances)
                    try:
                        classification[current_class,current_sample] = np.exp(distances-current_max)/np.sum(np.exp(distances-current_max))
                    except FloatingPointError:
                        classification[current_class,current_sample] = np.exp(distances-current_min)/np.sum(np.exp(distances-current_min))

                truth.append(ground_truth[current_class])
                classified_class = self.class_names[np.argmax(classification[current_class,current_sample])]
                if(classified_class == ground_truth[current_class]):
                    correct += 1
                print classification[current_class,current_sample], classified_class
            print 'Class success rate: ', correct, '/', np.shape(classify)[1]

        for index in range(0,len(ground_truth)):
            print collisions[index], ' collissions in ', ground_truth[index]
        np.seterr(all='warn')
        truth = np.array(truth)

        if(save_to != None):
            if(external_data != None):
                mock_names = []
                for index in range(0,np.shape(classification[1])):
                    mock_names.append('mystery_item'+str(index))
                mock_names = np.array(mock_names)
                    
                save_classifcation_to_h5(save_to,classification,ground_truth,mock_names)
            else:
                save_classifcation_to_h5(save_to,classification,self.validation_class_names,self.validation_sample_names)
            
        return classification, truth

    def return_train_test_data(self,feature_fraction=1.):

        x_train = np.reshape(self.feature_weights,(np.shape(self.feature_weights)[0]*np.shape(self.feature_weights)[1],np.shape(self.feature_weights)[2]))
        x_test = np.reshape(self.validation_weights,(np.shape(self.validation_weights)[0]*np.shape(self.validation_weights)[1],np.shape(self.validation_weights)[2]))

        if(feature_fraction < 1.):
            mask = self.fisher_weights > np.percentile(self.fisher_weights,(1.-feature_fraction)*100) 
            x_train = x_train[:,mask]
            x_test = x_test[:,mask]

        y_train = np.zeros([self.num_classes*self.num_samples],dtype='int')
        for index in range(0,self.num_classes):
            y_train[index*self.num_samples:(index+1)*self.num_samples] = index
        y_train = to_categorical(y_train,num_classes=self.num_classes)
        

        y_test = np.zeros([self.validation_classes*self.validation_samples],dtype='int')
        for index in range(0,self.validation_classes):
            y_test[index*self.validation_samples:(index+1)*self.validation_samples] = index
        y_test = to_categorical(y_test,num_classes=self.validation_classes)

        return x_train, y_train, x_test, y_test
