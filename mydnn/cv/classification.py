import numpy as np
import numpy.ma as ma
import h5py as h5

def load_from_file(filename,labels, grp='/'):
    h5file = h5.File(filename,mode='r')
    starting_point = h5file[grp]
    data = []
    ground_truth = []
    for current_name in starting_point:
        if(np.max(str(current_name) == labels)):
            current_grp = starting_point[current_name]
            for d in current_grp:
                dset = current_grp[d]
                if(np.shape(dset)[0] == len(labels)):
                    data.append(dset)
                    ground_truth.append(current_name)

    data = np.array(data)
    ground_truth = np.array(ground_truth)
    construct = classification(data,labels,input_truth=ground_truth)

    return construct


class classification(object):
    
    def __init__(self,classification_data,labels,input_truth=(None,None)):
        input_truth = np.array(input_truth)
        self.num_classes = len(labels)
        self.num_samples = len(classification_data)
        if(np.shape(classification_data)[1] != self.num_classes):
            raise RuntimeError('Number of classes do not match.')
        if(input_truth[0] != None):
            if(len(input_truth) != self.num_samples):
                raise ValueError('Groundtruth length does not match.')
            if( not np.array_equal(np.unique(labels),np.unique(input_truth))):
                raise ValueError('Some inconsistent class labels')

            self.report_labels = labels
        else:
            self.report_labels = np.array(['mystery_class'],dtype='string')

        self.ground_truth = input_truth
        self.data = classification_data
        self.labels = labels

        if(self.ground_truth[0] != None):
            self.numbers = np.zeros((self.num_classes,self.num_classes),dtype='int')
            self.means = np.zeros((self.num_classes,self.num_classes),dtype='float')
            self.stdevs = np.zeros((self.num_classes,self.num_classes),dtype='float')
            self.class_samples = np.zeros([self.num_classes],dtype='int')
            self.class_metrics = np.zeros([self.num_classes+1,3],dtype='float')
        else:
            self.numbers = np.zeros((1,self.num_classes),dtype='int')
            self.means = np.zeros((1,self.num_classes),dtype='float')
            self.stdevs = np.zeros((1,self.num_classes),dtype='float')
            self.class_samples = np.array([self.num_samples])
            self.class_metrics = None

    def execute(self,verbose=False):

            if(self.ground_truth[0] != None):
                for class_index in range(0,len(self.labels)):
                    mask = self.ground_truth == self.labels[class_index]
                    current_set = self.data[mask]
                    self.class_samples[class_index] = len(current_set)
                    winners = np.argmax(current_set,axis=1)
                    for class_indexclass_index in range(0,len(self.labels)):
                        maskmask = winners == class_indexclass_index
                        current_winners = winners[maskmask]
                        self.numbers[class_index,class_indexclass_index] = len(current_winners)
                        self.means[class_index,class_indexclass_index] = np.mean(current_set[:,class_indexclass_index])
                        self.stdevs[class_index,class_indexclass_index] = np.std(current_set[:,class_indexclass_index])
                    self.class_metrics[class_index+1,2] = -np.sum(np.log(current_set[:,class_index]))
                    self.class_metrics[0,2] += np.sum(np.log(current_set[:,class_index]))
                total = np.sum(self.class_samples)
                self.class_metrics[0,2] = -self.class_metrics[0,2]/total
                total_correct = 0
                for index in range(0,len(self.labels)):
                    total_correct += self.numbers[index,index]
                    self.class_metrics[index+1,0] = float(self.numbers[index,index])/self.class_samples[index]
                    self.class_metrics[index+1,1] = self.means[index,index]
                    self.class_metrics[index+1,2] /= self.class_samples[index]
                self.class_metrics[0,0] = float(total_correct)/float(total)
                self.class_metrics[0,1] = np.sum(self.class_metrics[1:,1])/len(self.labels)
                
                        
            else:
                winners = np.argmax(self.data,axis=1)
                for class_indexclass_index in range(0,len(self.labels)):
                    maskmask = (winners == class_indexclass_index)
                    current_winners = winners[maskmask]
                    self.numbers[0,class_indexclass_index] = len(current_winners)
                    self.means[0,class_indexclass_index] = np.mean(self.data[:,class_indexclass_index])
                    self.stdevs[0,class_indexclass_index] = np.std(self.data[:,class_indexclass_index])

    def print_summary(self):
        print 'This classifier knows the following classes: '
        for x in self.labels:
            print x

        print 'The classification set contains ', self.num_samples, ' samples.'
        if(self.ground_truth[0] == None):
            answer = 'not'
        else:
            answer = ''
        print 'The correct classification of the set is '+str(answer)+' known.'
        print '\n'

        for x in range(0,len(self.report_labels)):
            print '------------------------------'
            print 'Report on:', self.report_labels[x]
            print 'Subset contains ', self.class_samples[x], ' samples.'
            print 'They were classified as: '
            for y in range(0,len(self.labels)):
                print '{:12}'.format(self.labels[y]), '\t|\t', self.numbers[x][y], '/', self.class_samples[x] , '\t|\t', str(float(self.numbers[x][y])/self.class_samples[x] *100.)[0:4], '%' 
            print '\n'
            norm = np.max(self.means[x])
            winner = np.argmax(self.means[x])
            print 'What results in the following similarities: '
            for y in range(0,len(self.labels)):
                print '{:12}'.format(self.labels[y]), '\t|\t', self.means[x][y]/norm, '+/-', self.stdevs[x][y]/norm 
            print '\n'
            print 'The classifier thinks that this class is: ', self.labels[winner]
            print '------------------------------\n'
        

        if(self.ground_truth[0] != None):
            print '------------------------------'
            print 'Global accruacy report'
            print 'class | correct classifications  |  mean class probability | categorical cross entropy \n'

            print '{:12}'.format('all'), '\t|\t', self.class_metrics[0,0], '\t|\t', self.class_metrics[0,1], '\t|\t', self.class_metrics[0,2]

            for index in range(0,len(self.labels)):
                print '{:12}'.format(self.labels[index]), '\t|\t', self.class_metrics[index+1,0], '\t|\t', self.class_metrics[index+1,1], '\t|\t', self.class_metrics[index+1,2]
            
            print '------------------------------'

    

    def write_summary_to_ascii(self,filename):
        out = open(filename,'w')
        out.write( 'This classifier knows the following classes: \n')
        for x in self.labels:
            out.write(str(x)+'\n')

        out.write('The classification set contains '+str(self.num_samples)+' samples.\n')
        if(self.ground_truth[0] == None):
            answer = 'not'
        else:
            answer = ''
        out.write('The correct classification of the set is '+str(answer)+' known.\n\n')

        for x in range(0,len(self.report_labels)):
            out.write('------------------------------\n')
            out.write('Report on: '+str(self.report_labels[x])+'\n')
            out.write('Subset contains '+str(self.class_samples[x])+' samples.\n')
            out.write('They were classified as: \n')
            for y in range(0,len(self.labels)):
                out.write('{:12}'.format(self.labels[y])+'\t|\t'+str(self.numbers[x][y])+'/'+str(self.class_samples[x])+'\t|\t'+str(float(self.numbers[x][y])/self.class_samples[x] *100.)[0:4]+'%\n\n') 
            norm = np.max(self.means[x])
            winner = np.argmax(self.means[x])
            out.write('What results in the following similarities: \n')
            for y in range(0,len(self.labels)):
                out.write('{:12}'.format(self.labels[y])+'\t|\t'+str(self.means[x][y]/norm)+'+/-'+str(self.stdevs[x][y]/norm)+'\n\n') 
            out.write('The classifier thinks that this class is: '+str(self.labels[winner])+'\n')
            out.write('------------------------------\n')
        

        if(self.ground_truth[0] != None):
            out.write('------------------------------ \n')
            out.write('Global accruacy report \n')
            out.write('class | correct classifications  |  mean class probability | categorical cross entropy \n')

            out.write('{:12}'.format('all')+'\t|\t'+str(self.class_metrics[0,0])+'\t|\t'+str(self.class_metrics[0,1])+'\t|\t'+str(self.class_metrics[0,2])+'\n')

            for index in range(0,len(self.labels)):
                out.write('{:12}'.format(self.labels[index])+'\t|\t'+str(self.class_metrics[index+1,0])+'\t|\t'+str(self.class_metrics[index+1,1])+'\t|\t'+str(self.class_metrics[index+1,2])+'\n')
            
            out.write('------------------------------\n')
        
            
