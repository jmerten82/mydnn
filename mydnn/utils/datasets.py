import numpy as np
import h5py
import keras
from keras.utils import to_categorical, Progbar
from numpy.random import shuffle

def load_dataset(filename,classes=('lcdm'),channels=1,scale=False):

    #Open hdf5 input
    h5file = h5py.File(filename,'r')
    
    #Figuring out output sizes
    total_count = 0
    for x in classes:
        print 'Looking into group: ', x 
        current_grp = h5file[x]
        total_count += len(current_grp)

    #Some info to user
    print 'You choose to select ', total_count ,' images.'
        
    x_out = np.zeros((total_count,256,256,channels),dtype='float32')
    y_out = np.zeros((total_count),dtype='float32')    

    class_counter = 0
    running_counter = 0
    for x in classes:
        print 'Currently reading: ', x
        current_grp = h5file[x]
        dim = np.shape(current_grp)[0]
        image_counter = 0 
        for name in current_grp:
            if(image_counter%1000 == 0):
                print 'Already read:', image_counter
            img = current_grp[name]    
            if(len(np.shape(img)) > 2):
                img = img[:,:,0:channels]
            else:
                img = np.reshape(img,[np.shape(img)[0],np.shape(img)[0],1])

            if(scale):
                img = img - np.min(img)
                img = img/np.max(img)

            x_out[running_counter] = img
            y_out[running_counter] = float(class_counter)
            image_counter += 1
            running_counter += 1

        class_counter += 1

    y_out = to_categorical(y_out,num_classes=class_counter)

    return x_out, y_out

class Dustgrain_H5_datagenerator(keras.utils.Sequence):

    def __init__(self,h5file,sample='train',batch_size=128,classes = np.array(('f4','f4_03ev','f5','f5_015ev','f5_01ev','f6','f6_006ev','f6_01ev','lcdm')),channels=np.array((1,)),shuffle=True):
        
        self.h5file = h5py.File(h5file,'r')
        self.batch_size = batch_size
        self.classes = np.intersect1d(classes,self.h5file.keys())
        self.num_classes = len(self.classes)
        self.ids = self.h5file[self.classes[0]+'/'+sample+'/'].keys()
        self.ids = ['/'+self.classes[0]+'/'+sample+'/' + s for s in self.ids]
        self.labels = np.full((len(self.ids)),0,dtype='int')
        for i in range(1,self.num_classes):
            no_future = self.h5file[self.classes[i]+'/'+sample+'/'].keys()
            no_future = ['/'+self.classes[i]+'/'+sample+'/' + s for s in no_future]
            self.ids = np.append(self.ids,no_future)
            self.labels = np.append(self.labels,np.full(len(self.h5file[self.classes[i]+'/'+sample+'/'].keys()),i,dtype='int'))
        self.indeces = np.arange(0,len(self.ids))
        self.shuffle = shuffle
        if(self.shuffle):
            np.random.shuffle(self.indeces)
        self.channels = channels            
        self.x = np.shape(self.h5file[self.ids[self.indeces[0]]])[1]
        self.y = np.shape(self.h5file[self.ids[self.indeces[1]]])[2]
        self.c = len(self.channels)


    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.indeces) / self.batch_size))


    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indeces = self.indeces[index*self.batch_size:(index+1)*self.batch_size]

        # Generate data
        X, y = self.__data_generation(indeces)

        return X, y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indeces = np.arange(len(self.ids))
        if self.shuffle == True:
            np.random.shuffle(self.indeces)

    def __data_generation(self, indeces):
        'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
        # Initialization
        X = np.empty((self.batch_size,self.x,self.y,self.c))
        y = np.empty((self.batch_size), dtype=int)

        # Generate data
        running_index = 0
        for i in indeces:
            # Store sample
            for j in range(0,self.c):
                X[running_index,:,:,j] = self.h5file[self.ids[i]][self.channels[j],:,:]

            # Store class
            y[running_index] = self.labels[i]
            running_index += 1

        return X, keras.utils.to_categorical(y, num_classes=len(self.classes))


class MokaDeep_H5_datagenerator(keras.utils.Sequence):

    def __init__(self,h5file,redshift_label='z0.25',sample='train',batch_size=128,labels = np.array(('virial mass', 'concentration', 'BCG stellar mass', 'smooth halo concentration', 'subhalo fraction', 'number of subhaloes', 'a', 'b', 'c', 'theta', 'psi')),channels=np.array((0,1,2)),shuffle=True):
        
        max_labels = np.array(('virial mass', 'concentration', 'BCG stellar mass', 'smooth halo concentration', 'subhalo fraction', 'number of subhaloes', 'a', 'b', 'c', 'theta', 'phi'))
        self.label_index_dict = dict([(max_labels[0],0),(max_labels[1],1),(max_labels[2],2),(max_labels[3],3),(max_labels[4],4),(max_labels[5],5),(max_labels[6],6),(max_labels[7],7),(max_labels[8],8),(max_labels[9],9),(max_labels[10],10)])

        self.h5file = h5py.File(h5file,'r')
        self.batch_size = batch_size
        self.redshift_label = redshift_label
        self.ids = self.h5file['/'+self.redshift_label+'/'+sample+'/'].keys()
        self.ids = ['/'+self.redshift_label+'/'+sample+'/' + s for s in self.ids]
        self.label_labels = np.empty((0,),dtype='S25')
        for item in labels:
            if item in max_labels:
                self.label_labels = np.append(self.label_labels,item)

        self.indeces = np.arange(0,len(self.ids))
        self.shuffle = shuffle
        if(self.shuffle):
            np.random.shuffle(self.indeces)
        self.channels = channels  
        self.c = len(self.channels)
        self.l = len(self.label_labels)          

        if(self.redshift_label=='z0.25'):
            self.shifts = np.array((3.16900001186e+14,1.57356214523,169869721600.0,0.108719222248,0.162754699588,0.0,0.36287432909,0.472025305033,1.09357833862,0.22862316668,0.00932352617383))
            self.scales = np.array((2.7451000273e+15,11.2387919426,3.21964066406e+12,13.0881286934,0.837245300412,8146.0,0.561419308186,0.780282467604,4.5733704567,179.396529421,359.984084677))
        elif(self.redshift_label=='z0.5'):
            self.shifts = np.array((0,0,0,0,0,0,0,0,0,0,0))
            self.scales = np.array((1,1,1,1,1,1,1,1,1,1,1))
        elif(self.redshift_label=='z0.75'):
            self.shifts = np.array((0,0,0,0,0,0,0,0,0,0,0))
            self.scales = np.array((1,1,1,1,1,1,1,1,1,1,1))
        else:
            self.shifts = np.array((0,0,0,0,0,0,0,0,0,0,0))
            self.scales = np.array((1,1,1,1,1,1,1,1,1,1,1))

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.indeces) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indeces = self.indeces[index*self.batch_size:(index+1)*self.batch_size]

        # Generate data
        X, y = self.__data_generation(indeces)

        return X, y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indeces = np.arange(len(self.ids))
        if self.shuffle == True:
            np.random.shuffle(self.indeces)

    def __data_generation(self, indeces):
        'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
        # Initialization
        X = np.empty((self.batch_size,256,256,self.c),dtype='float')
        y = np.empty((self.batch_size,self.l), dtype='float')

        # Generate data
        running_index = 0
        for i in indeces:
            # Store sample
            for j in range(0,self.c):
                X[running_index,:,:,j] = self.h5file[self.ids[i]][:,:,self.channels[j]]

            for j in range(0,self.l):
                y[running_index,j] = (self.h5file[self.ids[i]].attrs[self.label_labels[j]] - self.shifts[self.label_index_dict[self.label_labels[j]]]) / self.scales[self.label_index_dict[self.label_labels[j]]]

            running_index += 1

        return X, y

    def return_data(self):

        dim = len(self.ids)
        X = np.empty((dim,256,256,self.c),dtype='float')
        y = np.empty((dim,self.l), dtype='float')

        proggy = Progbar(target=dim)
        running_index = 0

        for i in self.indeces:
            proggy.update(running_index)

            for j in range(0,self.c):
                X[running_index,:,:,j] = self.h5file[self.ids[i]][:,:,self.channels[j]]

            for j in range(0,self.l):
                y[running_index,j] = (self.h5file[self.ids[i]].attrs[self.label_labels[j]] - self.shifts[self.label_index_dict[self.label_labels[j]]]) / self.scales[self.label_index_dict[self.label_labels[j]]]

            running_index += 1

        return X, y
