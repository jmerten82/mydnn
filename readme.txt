readme.txt
Author: Julian Merten (INAF-OAS Bologna, julian.merten@inaf.it, http://www.julianmerten.net)

The purpose of this repository is twofold:

a) To collect and share our tools, algorithms and models for machine (and most deep) learning. To be found in ./mydnn.  This is by no means complete and still in development, esp. the GAN classes are just placeholders. 

b) To provide a repository for reproducible science on our research papers. This can be found in ./reproducible_science

  
